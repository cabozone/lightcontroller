import time
import csv
import datetime
from pygame import mixer
from pylibftdi import BitBangDevice
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import random

mixer.init()
time1 = time.localtime()
action_triggered = False
relay = BitBangDevice()
ch_white = 1
ch_blue = 2
ch_green = 4
ch_red = 8
ch_ice = 16
ch_tree = 32
ch_net = 64
ch_stars = 128
to_mail = 'to-email@gmail.com'
from_mail = 'gmail-user@gmail.com'

def notify(msg):
    payload = MIMEMultipart()
    payload['From'] = from_mail
    payload['To'] = to_mail
    payload['Subject'] = 'Light Notify ' + str(time1.tm_hour) + '-' + str(time1.tm_min)
    payload.attach(MIMEText(msg, 'plain'))
    s = smtplib.SMTP('smtp.gmail.com:587')
    s.starttls()
    s.login(from_mail,'email-password')
    text = payload.as_string()
    s.sendmail(from_mail, to_mail, text)
    s.quit()


def set_idle(hr):
    if hr == 12:
        relay.port = ch_white
    if hr == 18:
        relay.port = int(random.random()*128)+1
    if hr == 19:
        relay.port = int(random.random()*128)+1
    if hr == 20:
        relay.port = ch_green
    if hr == 21:
        relay.port = ch_blue
    if hr == 0:
        relay.port = 0
    return

def play_chime():
    action_next = "Play Chime"
    print action_next
    mixer.music.load("/home/pi/Documents/LightController/bell.mp3")
    if (time1.tm_hour > 12 and time1.tm_hour < 22):
        chime_count = time1.tm_hour-12
        notify('Chime Triggered')
        if time1.tm_min > 0:
            chime_count = 1
        while chime_count > 0:
            print chime_count
            mixer.music.play()
            relay.port = 128
            time.sleep(2)
            relay.port = 0
            time.sleep(2)
            chime_count=chime_count-1
        action_triggered = False
        set_idle(time1.tm_hour)
        time.sleep(60)
    return

def play_song(track):
    action_next = "Playing Song"
    notify('Song Triggered')
    if (track == 1):
        media1 = "/home/pi/Documents/LightController/jingle-bells.mp3"
        sequence1 = "/home/pi/Documents/LightController/jingle-bells.csv"
    if (track == 2):
        media1 = "/home/pi/Documents/LightController/o-holy-night.mp3"
        sequence1 = "/home/pi/Documents/LightController/o-holy-night.csv"
    print action_next
    with open(sequence1, "rb") as f:
        r = csv.reader(f)
        l = list(r)
    l_rows = len(l)
    mixer.music.load(media1)
    mixer.music.play()
    time.sleep(.8)
    for x in range(0,l_rows):
        lights = int(l[x][0]) + int(l[x][1]) + int(l[x][2]) + int(l[x][3]) + int(l[x][4]) + int(l[x][5]) + int(l[x][6]) + int(l[x][7])
        relay.port = lights
        time.sleep(.049)
    action_triggered = False
    set_idle(time1.tm_hour)
    return

notify('System Startup')
print "System Start"
while True:
   # print "X"
    time.sleep(2)
    time1 = time.localtime()
    if (time1.tm_hour >= 12 and time1.tm_hour <= 21):
        # print "."
        if (time1.tm_min == 0 or time1.tm_min == 15 or time1.tm_min == 30 or time1.tm_min == 45): 
            action_next = "chime"
            play_chime()
        if (time1.tm_min == 9 or time1.tm_min == 39):
            action_next = "song"
            play_song(1)
        if (time1.tm_min == 19 or time1.tm_min == 49):
            action_next = "song"
            play_song(2)

    else:
        set_idle(0)



